<%@ page import="com.example.jjob.model.JobApplication" %>
<%@ page import="java.util.List" %>
<%@ page import="com.example.jjob.model.Company" %>
<%@ page import="com.example.jjob.model.Application" %>
<%@include file="./master/header.jsp" %>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<div class="container pb-4 ">
    <div class="row ">


        <div class="col-12 mt-4 ">
            <nav class="navbar navbar-light bg-light">
                <div class="container-fluid justify-content-center">
                    <a class="navbar-brand" href="#">
                        Job Application
                    </a>
                </div>
            </nav>
        </div>

        <h3 class="m-3">Timeline</h3>

        <div class="col-12 mt-3 bg-transparent">

            <label>Select Company</label>
            <select id="selectCompany" onchange="changeCompany()" class="form-control mb-3">
                <option value="all"></option>
                <option value="all">All</option>
                <%
                    List<Company> companies = (List<Company>) request.getAttribute("companies");
                    for (Company company : companies) {

                %>
                <option value="<%=company.getName()%>"><%=company.getName()%></option>
                <%
                    }
                %>
            </select>

            <script>
                function changeCompany() {
                    var x = document.getElementById("selectCompany").value;
                    console.log(x);
                    window.location = "/job/byCompany?company="+x;
                }
            </script>
            <div>
                <div class="card bg-transparent modal-dialog-scrollable">
                    <div class="card-header">

                    </div>
                    <div class="card-body">
                        <blockquote class="blockquote mb-0">


                        </blockquote>

                        <div class="login m-0 p-0">
                            <div class="card d-none ">
                                <h5 class="card-header">Featured</h5>
                                <div class="card-body">
                                    <h5 class="card-title">Special title treatment</h5>
                                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>


                        <%
                            List<Application> applications = (List<Application>) request.getAttribute("applications");

                            for (Application application1 : applications) {

                        %>

                        <div class="card mb-3">

                            <h5 class="p-3">
                                <div class="row">
                                    <div class="col-4 p-2">
                                        <b>Company - <%=application1.getJobApplication().getCompany().getName()%></b>
                                    </div>
                                    <div class="col-4 p-2">
                                        Job Title - <%=application1.getJobApplication().getTitle()%>
                                    </div>
                                    <div class="col-4 p-2">
                                        Salary: <%=application1.getJobApplication().getSalaryRange()%>
                                    </div>
                                </div>
                            </h5>
                            <div class="card-body">
                                <h5 class="card-title"><%=application1.getUser().getFirstName()%> <%=application1.getUser().getLastName()%></h5>
                                <p class="card-text">Experience: <%=application1.getUser().getExpYear()%></p>
                                <a href="#" class="btn btn-primary">Email: <%=application1.getUser().getEmail()%></a>
                            </div>
<%--                            <hr style="border: 1px solid black"/>--%>
<%--                            <div class="card-body">--%>
<%--                                <h5 class="card-title">User</h5>--%>
<%--                                <p class="card-text">Exp</p>--%>
<%--                                <a href="#" class="btn btn-primary">Go somewhere</a>--%>
<%--                            </div>--%>
                        </div>

                        <%
                            }
                        %>

                    </div>


                </div>
            </div>

        </div>


    </div>
</div>

</body>
</html>