<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<%--    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">--%>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">


    <style type="text/css">
        body {
            background-color: rgba(15, 93, 101, 0.16);
            font-family: "Asap", sans-serif;
        }

        .login {
            overflow: hidden;
            background-color: white;
            padding: 40px 30px 30px 30px;
            border-radius: 10px;
            position: fixed;
            /*top: 30%;*/
            left: 15%;
            /*width: 400px;*/
            /*-webkit-transform: translate(-50%, -50%);*/
            /*-moz-transform: translate(-50%, -50%);*/
            /*-ms-transform: translate(-50%, -50%);*/
            /*-o-transform: translate(-50%, -50%);*/
            /*transform: translate(-50%, -50%);*/
            /*-webkit-transition: -webkit-transform 300ms, box-shadow 300ms;*/
            /*-moz-transition: -moz-transform 300ms, box-shadow 300ms;*/
            transition: transform 300ms, box-shadow 300ms;
            box-shadow: 5px 10px 10px rgba(2, 128, 144, 0.2);
        }
        .login::before, .login::after {
            content: "";
            position: fixed;
            width: 600px;
            height: 600px;
            border-top-left-radius: 40%;
            border-top-right-radius: 45%;
            border-bottom-left-radius: 35%;
            border-bottom-right-radius: 40%;
            z-index: -1;
        }
        .login::before {
            /*left: 40%;*/
            /*bottom: -130%;*/
            background-color: rgba(69, 105, 144, 0.15);
            -webkit-animation: wawes 6s infinite linear;
            -moz-animation: wawes 6s infinite linear;
            animation: wawes 6s infinite linear;
        }
        .login::after {
            /*left: 35%;*/
            /*bottom: -125%;*/
            background-color: rgba(2, 128, 144, 0.2);
            -webkit-animation: wawes 7s infinite;
            -moz-animation: wawes 7s infinite;
            animation: wawes 7s infinite;
        }

        @-webkit-keyframes wawes {
            from {
                -webkit-transform: rotate(0);
            }
            to {
                -webkit-transform: rotate(360deg);
            }
        }
        @-moz-keyframes wawes {
            from {
                -moz-transform: rotate(0);
            }
            to {
                -moz-transform: rotate(360deg);
            }
        }
        @keyframes wawes {
            from {
                -webkit-transform: rotate(0);
                -moz-transform: rotate(0);
                -ms-transform: rotate(0);
                -o-transform: rotate(0);
                transform: rotate(0);
            }
            to {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        /*a {*/
        /*    text-decoration: none;*/
        /*    color: rgba(255, 255, 255, 0.6);*/
        /*    position: absolute;*/
        /*    right: 10px;*/
        /*    bottom: 10px;*/
        /*    font-size: 12px;*/
        /*}*/
    </style>

    <title>JOB APPLICATION</title>
</head>
<body>