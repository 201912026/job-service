<%@ page import="com.example.jjob.model.JobApplication" %>
<%@ page import="java.util.List" %>
<%@include file="./master/header.jsp" %>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


<div class="container pb-4 login">
    <div class="row ">


        <div class="col-12 mt-4 ">
            <nav class="navbar navbar-light bg-light">
                <div class="container-fluid justify-content-center">
                    <a class="navbar-brand" href="#">
                        Job Application
                    </a>
                </div>
            </nav>
        </div>

        <h3 class="m-3">Create New Job </h3>

        <form class="col-12 mt-3 bg-transparent" method="post" action="/job/saveJob">
            <div>
                <div class="card bg-transparent">
                    <div class="card-header">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Job Title</label>
                            <input type="text" class="form-control" name="jobTitle" id="exampleInputEmail1"
                                   aria-describedby="emailHelp">
                        </div>
                    </div>
                    <div class="card-body">
                        <blockquote class="blockquote mb-0">

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <!--                                    <label for="inputEmail4">Email</label>-->
                                    <input type="text" placeholder="Country" name="country" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <!--                                    <label for="inputPassword4">Password</label>-->
                                    <input type="text" placeholder="City" name="city" class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input type="text" placeholder="State" name="state" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" placeholder="Experience Range" name="exp" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input type="text" placeholder="Salary Range" name="salary" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" placeholder="Job Type" name="type" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input type="text" placeholder="Job Description" name="desc" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" placeholder="Extra Information" name="extra" class="form-control">
                                </div>
                            </div>
                        </blockquote>

                        <div class="row mt-3">
                            <div class="col-2"></div>
                            <div class="col-8">
                                <button type="submit" class="btn btn-lg btn-block btn-success">Create Job</button>
                            </div>
                            <div class="col-2"></div>
                        </div>

                    </div>


                </div>
            </div>

        </form>


    </div>
</div>

</body>
</html>