<%@ page import="com.example.jjob.model.JobApplication" %>
<%@ page import="java.util.List" %>
<%@include file="./master/header.jsp" %>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<div class="container pb-4">
    <form method="post" action="/job/application" class="row">


        <div class="col-12 mt-4">
            <nav class="navbar navbar-light bg-light">
                <div class="container-fluid justify-content-center">
                    <a class="navbar-brand" href="#">
                        Job Application
                    </a>
                </div>
            </nav>
        </div>

        <%
            if(request.getSession().getAttribute("userType").equals("recruiter")) {
        %>
        <h4>
        <a class="m-3 btn-link" href="/job/createJob">Create Job</a>
        </h4>
        <h3 class="m-3">Job Applications</h3>

        <%
            }
        %>
        <%
            List<JobApplication> list = (List<JobApplication>) request.getAttribute("jobList");
            if(list.size()!=0) {
                for(JobApplication job : list) {
        %>

        <div class="col-12 mt-3">
            <div class="login m-0 p-0">
                <div class="card d-none ">
                    <h5 class="card-header">Featured</h5>
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-5 p-2 fw-bolder">
                            <b>Company - <%=job.getCompany().getName()%></b>
                        </div>
                        <div class="col-5 p-2">
                            <%=job.getTitle()%>
                        </div>
                        <div class="col-2">
                            <a class="button btn btn btn-success" href="/job/application/<%=job.getId()%>" >Apply</a>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <blockquote class="blockquote mb-0">
                        <p><%=job.getDesc()%></p>
                        <p class="alert alert-info"><%=job.getExtraInfo()%></p>
                        <footer class="blockquote-footer">Salary: <%=job.getSalaryRange()%> </footer>
                        <footer class="blockquote-footer">Exp: <%=job.getExpRange()%> </footer>
                    </blockquote>
                </div>
            </div>
        </div>

            <%
                }
}
            %>

    </form>
</div>

</body>
</html>
