package com.example.jjob.controller;

import com.example.jjob.enums.status;
import com.example.jjob.model.Application;
import com.example.jjob.model.Company;
import com.example.jjob.model.JobApplication;
import com.example.jjob.model.User;
import com.example.jjob.services.ApplicationService;
import com.example.jjob.services.CompanyService;
import com.example.jjob.services.JobApplicationService;
import com.example.jjob.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("job")
public class JobApplicationController {

    private static final Logger log = LoggerFactory.getLogger(JobApplicationController.class);

    @Autowired
    private CompanyService companyService;

    @Autowired
    private UserService userService;

    @Autowired
    private JobApplicationService applicationService;

    @Autowired
    private ApplicationService service;

    @RequestMapping("saveJob")
    public String index(Model model, HttpServletResponse response, HttpServletRequest request) {
        log.info("Hello");
        String country = request.getParameter("country");
        String city = request.getParameter("city");
        String desc = request.getParameter("desc");
        String jobTitle = request.getParameter("jobTitle");
        String state = request.getParameter("state");
        String exp = request.getParameter("exp");
        String salary = request.getParameter("salary");
        String type = request.getParameter("type");
        String extra = request.getParameter("extra");

        log.info("info {} {} {} {} {} {} {} {} {}", country, city, desc, jobTitle, state, exp, salary, type, extra);

        if (request.getSession().getAttribute("userType").equals("recruiter")) {

            User sessionUser = (User) request.getSession().getAttribute("user");
            Company sessionCompany = (Company) request.getSession().getAttribute("company");

            JobApplication application = new JobApplication();
            application.setActive(true);
            application.setCity(city);
            application.setState(state);
            application.setCountry(country);
            application.setDesc(desc);
            application.setExpRange(exp);
            application.setExtraInfo(extra);
            application.setJobType(type);
            application.setPostBy(sessionUser);
            application.setTitle(jobTitle);
            application.setSalaryRange(salary);
            application.setCompany(sessionCompany);
            applicationService.save(application);


//            return "manageJob";

            List<JobApplication> list = applicationService.getAllJobs();
            for (JobApplication applications : list) {
                log.info(applications.getTitle());
            }

            request.setAttribute("jobList", list);
            model.addAttribute("jobList", list);

            return "home";

        }

        List<JobApplication> list = applicationService.getAllJobs();
        for (JobApplication application : list) {
            log.info(application.getTitle());
        }

        request.setAttribute("jobList", list);
        model.addAttribute("jobList", list);

        return "home";

    }

    @RequestMapping("application/{id}")
    public String applicationSubmit( @PathVariable Long id, Model model, HttpServletResponse response, HttpServletRequest request) {
        log.info("application submit");
        log.info(id.toString());

        User sessionUser = (User) request.getSession().getAttribute("user");
        JobApplication jobApplication = applicationService.getById(id);

        Application application = new Application();
        application.setJobApplication(jobApplication);
        application.setDateCreated(new Date());
        application.setUser(sessionUser);
        application.setStatus(status.INITIALIZE);
        service.save(application);

        List<Application> applications = service.getAll();
        request.setAttribute("applications",applications);

        List<Company> companies = companyService.getAll();
        request.setAttribute("companies",companies);

        return "manageApplication";
    }


    @RequestMapping("byCompany")
    public String showByCompany(@RequestParam String company, HttpServletResponse response, HttpServletRequest request) {

        log.info(company);

        List<Application> applications = service.getAllByCompany(company);
        if(company.equalsIgnoreCase("all")) {
            applications = service.getAll();
        }

        request.setAttribute("applications",applications);

        List<Company> companies = companyService.getAll();
        request.setAttribute("companies",companies);

        return "manageApplication";

    }

    @RequestMapping("createJob")
    public String showCreateJob(Model model, HttpServletResponse response, HttpServletRequest request) {
        return "createJob";
    }

}
