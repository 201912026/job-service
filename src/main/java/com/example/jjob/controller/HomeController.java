package com.example.jjob.controller;

import com.example.jjob.model.Company;
import com.example.jjob.model.JobApplication;
import com.example.jjob.model.User;
import com.example.jjob.services.ApplicationService;
import com.example.jjob.services.CompanyService;
import com.example.jjob.services.JobApplicationService;
import com.example.jjob.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;

@Controller
public class HomeController {

    private static final Logger log = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private CompanyService companyService;

    @Autowired
    private UserService userService;

    @Autowired
    private JobApplicationService applicationService;

    boolean firstTime = true;

    @RequestMapping("/")
    public String index(Model model, HttpServletResponse response, HttpServletRequest request) {
        log.info("calling");

        if (firstTime) {
            createEntry();
        }
        firstTime = false;
        return "index";
    }


    @PostMapping("login")
    public String login(Model model, HttpServletRequest request, HttpServletResponse response) {

        if(firstTime){
            createEntry();
        }

        String userName = request.getParameter("userName");
        String password = request.getParameter("password");
//        log.info(userName);
//        log.info(password);

        User verifyUser = userService.findByEmailId(userName);
        if(Objects.nonNull(verifyUser)) {

            log.info("user: {}", verifyUser.getEmail());

            if(userService.verifyUser(verifyUser, password)) {

                log.info("verified");

                request.getSession().setAttribute("user", verifyUser);

                if(verifyUser.isRecruiter()) {
                    request.getSession().setAttribute("userType", "recruiter");
                    request.getSession().setAttribute("company", verifyUser.getCompany());
                    // recruiter


                    List<JobApplication> list = applicationService.getAllJobs();
                    for (JobApplication application : list) {
                        log.info(application.getTitle());
                    }

                    request.setAttribute("jobList", list);
                    model.addAttribute("jobList", list);

                    return "home";

                } else {
                    request.getSession().setAttribute("userType", "user");
                    // user

                    List<JobApplication> list = applicationService.getAllJobs();
                    for (JobApplication application : list) {
                        log.info(application.getTitle());
                    }

                    request.setAttribute("jobList", list);
                    model.addAttribute("jobList", list);

                    return "home";

                }

            } else {
                return "index";
            }

        }
            return "index";

    }

    public void createEntry() {

        Company company = new Company();
        company.setName("Tailshire");
        company.setAddress("Pune");
        company.setDescription("Pet Resource");
        company.setNoOfEmployees("50");
        company.setWebsite("www.tailshire.com");
        companyService.save(company);

        company = companyService.findByName(company.getName());

        log.info("company: {} ", company.getId());
        Company company2 = new Company();
        company2.setName("Microsoft");
        company2.setAddress("Bangalore");
        company2.setDescription("Production");
        company2.setNoOfEmployees("50,0000");
        company2.setWebsite("www.microsoft.com");
        companyService.save(company2);

        company2 = companyService.findByName(company2.getName());
        log.info("company: {} ", company2.getId());

        User user = new User();
        user.setCompany(company);
        user.setEmail("company@email.com");
        user.setFirstName("zain");
        user.setLastName("Mansuri");
        user.setPassword("password");
        user.setRecruiter(true);
        userService.save(user);

        user = userService.findByEmailId(user.getEmail());
        log.info("one, {}",user.getId().toString());

        User user2 = new User();
        user2.setCompany(company2);
        user2.setEmail("company2@email.com");
        user2.setFirstName("Company");
        user2.setLastName("2");
        user2.setPassword("password");
        user2.setRecruiter(true);
        userService.save(user2);

        user = userService.findByEmailId(user2.getEmail());
        log.info("two, {}",user.getFirstName());


        User user3 = new User();
//            user2.setCompany(company2);
        user3.setEmail("user@email.com");
        user3.setFirstName("User");
        user3.setLastName("User");
        user3.setPassword("password");
        user3.setRecruiter(false);
        userService.save(user3);

        user = userService.findByEmailId(user3.getEmail());
        log.info("three, {}",user.getFirstName());

        JobApplication application = new JobApplication();
        application.setActive(true);
        application.setCity("AHmedabad");
        application.setCountry("India");
        application.setDesc("Job DESC");
        application.setExpRange("0-8Y");
        application.setExtraInfo("No Extra Infor");
        application.setCountry("India");
        application.setJobType("PERMANENT");
        application.setPostBy(user);
        application.setTitle("Software Engineer");
        application.setSalaryRange("8-10L");
        application.setCompany(company);
        applicationService.save(application);


        JobApplication application2 = new JobApplication();
        application2.setActive(true);
        application2.setCity("Pune");
        application2.setDesc("Job DESC");
        application2.setExpRange("1-3Y");
        application2.setExtraInfo("Soo much info");
        application2.setCountry("India");
        application2.setJobType("PERMANENT");
        application2.setPostBy(user);
        application2.setCompany(company2);
        application2.setTitle("Developer");
        application2.setSalaryRange("7-14L");
        applicationService.save(application2);


    }


}
