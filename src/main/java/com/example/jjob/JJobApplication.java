package com.example.jjob;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JJobApplication {

    private static final Logger log = LoggerFactory.getLogger(JJobApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(JJobApplication.class, args);
    }
}
