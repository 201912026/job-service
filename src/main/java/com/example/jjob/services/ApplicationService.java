package com.example.jjob.services;

import com.example.jjob.model.Application;

import java.util.List;

public interface ApplicationService {

    void save(Application application);

    List<Application> getAll();

    List<Application> getAllByCompany(String company);

    Application getApplicationById(Long id);
}
