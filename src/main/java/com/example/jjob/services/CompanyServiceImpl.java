package com.example.jjob.services;

import com.example.jjob.model.Company;
import com.example.jjob.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public List<Company> getAll() {
        return companyRepository.getAll();
    }

    @Override
    public Company findById(Long id) {
        return companyRepository.getById(id);
    }

    @Override
    public Company findByName(String name) {
        return companyRepository.getByName(name);
    }

    @Override
    public void save(Company company) {
        companyRepository.save(company);
    }
}
