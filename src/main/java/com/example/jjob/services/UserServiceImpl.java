package com.example.jjob.services;

import com.example.jjob.model.User;
import com.example.jjob.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Override
    public List<User> getAll() {
        return repository.getAll();
    }

    @Override
    public List<User> getAllUser() {
        return repository.getAllUser();
    }

    @Override
    public List<User> getAllRecruiter() {
        return repository.getAllRecruiter();
    }

    @Override
    public void save(User user) {
        repository.save(user);
    }

    @Override
    public void updateOrSave(User user) {
        repository.save(user);
    }

    @Override
    public User findById(Long id) {
        return repository.getUsersById(id);
    }

    @Override
    public User findByEmailId(String id) {
        return repository.getUserByEmail(id);
    }

    @Override
    public boolean verifyUser(User user, String password) {
        if(Objects.nonNull(repository.verifyUser(user.getEmail(), password))) {
            return true;
        } else {
            return false;
        }
    }
}
