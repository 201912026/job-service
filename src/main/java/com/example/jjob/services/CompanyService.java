package com.example.jjob.services;

import com.example.jjob.model.Company;

import java.util.List;

public interface CompanyService {

    List<Company> getAll();

    Company findById(Long id);

    Company findByName(String name);

    void save(Company company);
}
