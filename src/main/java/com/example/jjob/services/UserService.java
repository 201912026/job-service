package com.example.jjob.services;

import com.example.jjob.model.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    List<User> getAllUser();

    List<User> getAllRecruiter();

    void save(User user);

    void updateOrSave(User user);

    User findById(Long id);

    User findByEmailId(String id);

    boolean verifyUser(User user, String password);

}
