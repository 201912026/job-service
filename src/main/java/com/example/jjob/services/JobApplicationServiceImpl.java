package com.example.jjob.services;

import com.example.jjob.model.JobApplication;
import com.example.jjob.repository.JobApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JobApplicationServiceImpl implements JobApplicationService {

    @Autowired
    private JobApplicationRepository repository;

    @Override
    public List<JobApplication> getAllJobs() {
        return repository.getAll();
    }

    @Override
    public List<JobApplication> getAllJobsByCompanyId(Long id) {
        return repository.getAllByCompanyId(id);
    }

    @Override
    public void save(JobApplication jobApplication) {
        repository.save(jobApplication);
    }

    @Override
    public JobApplication getById(Long id) {
        return repository.findById(id).orElse(new JobApplication());
    }
}
