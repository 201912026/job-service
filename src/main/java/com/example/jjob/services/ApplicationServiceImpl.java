package com.example.jjob.services;

import com.example.jjob.model.Application;
import com.example.jjob.repository.ApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApplicationServiceImpl implements ApplicationService {

    @Autowired
    private ApplicationRepository repository;

    @Override
    public void save(Application application) {
        repository.save(application);
    }

    @Override
    public List<Application> getAll() {
        return repository.gteAll();
    }

    @Override
    public List<Application> getAllByCompany(String company) {
        return repository.getAllByCompany(company);
    }

    @Override
    public Application getApplicationById(Long id) {
        return repository.findById(id).orElse(new Application());
    }
}
