package com.example.jjob.services;

import com.example.jjob.model.JobApplication;

import java.util.List;

public interface JobApplicationService {

    List<JobApplication> getAllJobs();

    List<JobApplication> getAllJobsByCompanyId(Long id);

    void save(JobApplication jobApplication);

    JobApplication getById(Long id);

}
