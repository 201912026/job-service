package com.example.jjob.repository;

import com.example.jjob.model.Company;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {

    @Query( value = "select c from Company c")
    List<Company> getAll();

    Company getById(Long id);

    Company getByName(String name);

}
