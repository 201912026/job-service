package com.example.jjob.repository;

import com.example.jjob.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    @Query( value = "select u from User u where u.isRecruiter = false")
    List<User> getAllUser();

    @Query( value = "select u from User u where u.isRecruiter = true")
    List<User> getAllRecruiter();

    @Query( value = "select u from User u")
    List<User> getAll();

    User getUsersById(Long id);

    @Query( value = "select u from User u where u.email like ?1")
    User getUserByEmail(String email);

    @Query( value = "select u from User u where u.email like ?1 and u.password like ?2")
    User verifyUser(String email, String pass);
}
