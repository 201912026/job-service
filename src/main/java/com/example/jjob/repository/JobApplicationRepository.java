package com.example.jjob.repository;

import com.example.jjob.model.JobApplication;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobApplicationRepository extends CrudRepository<JobApplication, Long> {

    @Query( value = "select jb from JobApplication jb")
    List<JobApplication> getAll();

    @Query( value = "select jb from JobApplication jb where jb.company.id = ?1")
    List<JobApplication> getAllByCompanyId(Long id);


}
