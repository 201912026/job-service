package com.example.jjob.repository;

import com.example.jjob.model.Application;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ApplicationRepository extends CrudRepository<Application, Long> {

    @Query( value = "select app from Application app")
    List<Application> gteAll();

    @Query( value = "select app from Application app where app.jobApplication.company.name like ?1")
    List<Application> getAllByCompany(String company);


}
